%global debug_package %{nil}

Name:          znapzend
Version:       0.22.0
Release:       1%{?dist}
Summary:       zfs backup with remote capabilities and mbuffer integration
License:       GPLv3+
Source:        https://github.com/oetiker/znapzend/releases/download/v%{version}/znapzend-%{version}.tar.gz
BuildArch:     noarch
BuildRequires: perl-core
BuildRequires: perl-generators
BuildRequires: perl-interpreter
BuildRequires: systemd autoconf automake
# For running 'make check'
BuildRequires: perl(Test::SharedFork)
BuildRequires: perl(Test::Exception)
BuildRequires: perl(Mojo::Log::Clearable)
BuildRequires: perl(Mojo::Base)
BuildRequires: perl(Role::Tiny)

%description
ZnapZend is a ZFS centric backup tool to create snapshots and send them to 
backup locations. It relies on the ZFS tools snapshot, send and receive to do 
its work. It has the built-in ability to manage both local snapshots as well as 
remote copies by thinning them out as time progresses.

%prep
%autosetup
autoreconf

%build
%configure --libdir %{perl_vendorlib}
# Hack: prevent installing stuff via CPAN
sed -i 's/thirdparty//g' Makefile 
PERL5_CPANPLUS_IS_RUNNING=1 %make_build

%install
%make_install

mkdir -p %{buildroot}%{_unitdir}
cp init/znapzend.service %{buildroot}%{_unitdir}/znapzend.service

%check
PERL5_CPANPLUS_IS_RUNNING=1 make check

%files
%doc README.md CHANGES
%license LICENSE COPYRIGHT
%{_bindir}/*
%{perl_vendorlib}/*
%{_mandir}/*/*
%{_unitdir}/*
